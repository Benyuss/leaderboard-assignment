package model;

public class Result implements Comparable<Result> {
    private String name;
    private int numberOfWins;
    private double winPercent;
    private int allKills;
    private double kd;
    private int topTen;
    private int games;

    public Result() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfWins() {
        return numberOfWins;
    }

    public void setNumberOfWins(int numberOfWins) {
        this.numberOfWins = numberOfWins;
    }

    public double getWinPercent() {
        return winPercent;
    }

    public void setWinPercent(double winPercent) {
        this.winPercent = winPercent;
    }

    public int getAllKills() {
        return allKills;
    }

    public void setAllKills(int allKills) {
        this.allKills = allKills;
    }

    public double getKd() {
        return kd;
    }

    public void setKd(double kd) {
        this.kd = kd;
    }

    public int getTopTen() {
        return topTen;
    }

    public void setTopTen(int topTen) {
        this.topTen = topTen;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    @Override
    public int compareTo(Result result) {
        return Double.compare(winPercent, result.getWinPercent());
    }

    @Override
    public String toString() {
        return "Result{" +
                "name='" + name + '\'' +
                ", numberOfWins=" + numberOfWins +
                ", winPercent=" + winPercent +
                ", allKills=" + allKills +
                ", kd=" + kd +
                ", topTen=" + topTen +
                ", games=" + games +
                '}';
    }
}
