package model;

/**
 * Model class that represent model.Score values from the parsed text file.
 */
public class Score implements Comparable<Score> {
    private String name;
    private int placement;
    private int kills;

    //kunstruktor
    public Score(String name, int placement, int kills) {
        this.name = name;
        this.placement = placement;
        this.kills = kills;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPlacement() {
        return placement;
    }

    public void setPlacement(int placement) {
        this.placement = placement;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    @Override
    public int compareTo(Score other) {
        return this.name.compareToIgnoreCase(other.name);
    }

    @Override
    public String toString() {
        return name + " " + placement + " " + kills;
    }
}
