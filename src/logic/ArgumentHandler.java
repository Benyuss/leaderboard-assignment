package logic;

import model.Score;

public class ArgumentHandler {

    public Score getScore(String[] args) {
        try {
            String name = args[0];
            String placement = args[1];
            String kills = args[2];
            return new Score(name, Integer.parseInt(placement), Integer.parseInt(kills));
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("\nYou have specified wrong arguments to add a new player.");
        }
        return null;
    }
}
