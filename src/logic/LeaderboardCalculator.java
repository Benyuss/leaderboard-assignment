package logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import model.Result;
import model.Score;

public class LeaderboardCalculator {

    public List<Result> calculateResultsFromScores(List<Score> scores) {
        Collections.sort(scores);
        return getResults(scores);
    }

    private List<Result> getResults(List<Score> scores) {
        List<Result> results = new ArrayList<>();

        for (Score score : scores) {
            //WE check if in the result list is there anyone with the same name as the current name.If the answer is yes we do not count his stats again.
            boolean resultsContainsPlayer = results.stream().filter(result -> result.getName().equals(score.getName())).collect(Collectors.toList()).size() != 0;
            if (!resultsContainsPlayer) {
                Result result = calculateResult(score, scores);
                results.add(result);
            }
        }
        return results;
    }

    private Result calculateResult(Score score, List<Score> scores) {
        Result result = new Result();
        result.setName(score.getName());

        for (Score currentScore : scores) {
            calculateScoreDynamicValues(result, currentScore);
        }
        calculateFurtherScores(result);
        return result;
    }

    private void calculateScoreDynamicValues(Result result, Score currentScore) {
        if (result.getName().equals(currentScore.getName())) {
            result.setGames(result.getGames() + 1);
            result.setAllKills(result.getAllKills() + currentScore.getKills());
            if (currentScore.getPlacement() == 1) {
                result.setNumberOfWins(result.getNumberOfWins() + 1);
            }
            if (currentScore.getPlacement() <= 10) {
                result.setTopTen(result.getTopTen() + 1);
            }
        }
    }

    private void calculateFurtherScores(Result result) {
        result.setWinPercent((double) result.getNumberOfWins() / result.getGames());
        int numberOfDeaths = result.getGames() - result.getNumberOfWins();

        //"your condition"? "step if true":"step if condition fails"
        result.setKd((double) result.getAllKills() / ((numberOfDeaths) == 0 ? 1 : numberOfDeaths));
    }

    public Result getAbsoluteBestPlayer(List<Result> results) {
        List<Result> resultsCopy = new ArrayList<>(results);
        Collections.sort(resultsCopy);
        return resultsCopy.get(0);
    }

    public Result getAbsoluteWorstPlayer(List<Result> results) {
        List<Result> resultsCopy = new ArrayList<>(results);
        Collections.sort(resultsCopy);
        return resultsCopy.get(results.size() - 1);
    }


}
