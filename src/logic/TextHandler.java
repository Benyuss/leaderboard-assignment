package logic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import model.Score;

public class TextHandler {

    public List<Score> scanFromInput(String fileName) throws FileNotFoundException {
        List<Score> scores = new ArrayList<>();

        // Declare File object
        File file = new File(fileName);
        // initialize the scanner
        Scanner scan = new Scanner(file);
        // iterate through the file line by line
        while (scan.hasNextLine()) {
            // print the token
            Score input = convertScore(scan.nextLine());
            scores.add(input);
        }
        scan.close();

        return scores;
    }

    private Score convertScore(String line) {
        Scanner scanner = new Scanner(line);
        scanner.useDelimiter(" ");
        if (scanner.hasNext()) {
            //assumes the line has a certain structure
            String name = scanner.next();
            String placement = scanner.next();
            String kill = scanner.next();
            return new Score(name, Integer.parseInt(placement), Integer.parseInt(kill));
        } else {
            throw new RuntimeException("Line can not be parsed.");
        }
    }

    public void writeToFile(String fileName, Score score) throws IOException {
        // Declare File object
        File file = new File(fileName);

        Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(file, true), StandardCharsets.UTF_8));

        writer.write(System.lineSeparator());
        writer.write(score.toString());

        writer.close();
    }
}
