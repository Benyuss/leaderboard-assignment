package logic;

import java.io.IOException;
import java.util.List;

import model.Result;
import model.Score;

public class Leaderboard {

    public static void main(String[] args) {
        Leaderboard leaderboard = new Leaderboard();
        TextHandler textHandler = new TextHandler();

        String fileName = "B:\\Dokumentumok\\VCS\\Leaderboard\\input.txt";

        try {
            leaderboard.addScore(args, textHandler, fileName);

            List<Score> scores = textHandler.scanFromInput(fileName);

            LeaderboardCalculator calculator = new LeaderboardCalculator();
            List<Result> results = calculator.calculateResultsFromScores(scores);
            leaderboard.printResults(results, calculator.getAbsoluteBestPlayer(results), calculator.getAbsoluteWorstPlayer(results));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Specified file can not be found.");
        }
    }

    private void printResults(List<Result> results, Result bestPlayer, Result worstPlayer) {
        System.out.println("Results are the following\n");
        for (Result result : results) {
            System.out.println(result.toString());
        }
        System.out.println("\nThe absolute best player was:");
        System.out.println(bestPlayer.toString());
        System.out.println("\nThe absolute worst player was:");
        System.out.println(worstPlayer.toString());
    }

    private void addScore(String[] args, TextHandler textHandler, String fileName) throws IOException {
        ArgumentHandler argumentHandler = new ArgumentHandler();
        Score newScore = argumentHandler.getScore(args);

        if (newScore != null) {
            textHandler.writeToFile(fileName, newScore);
        }
    }
}